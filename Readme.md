<h1>Read Me</h1>
<h6>December 2018</h6>
<h7>Author: Ricardo Molina</h7>

<h1> Creating the pods </h1>

<h4>Step 1</h1>

Run the following command.

<code>$kubectl -n <namespace> create -f vol.yaml

Then run the command 

<code>$kubectl -n <namespace> get pvc

you should get an output showing you that the test-vol is created


<h4>Step 2</h4>

run the following command

<code>$kubectl -n <namespace> create -f  unityservice.yaml</code>

run the next command 

<code>kubectl -n <name space> get pods</code>

you should see an output similar to the one below 

```
       Name      Ready  Status    Restarts     Age 
    <Pod Name>    1/1   Running      0          5 
```

<h1> Setting up the port forwarding and getting into unity
<h4>Step 1</h4>

Enter in the following command. 

<code>$kubectl -n <name space> get pods</code>

Now you should see an output that is similar to the one below.

```
       Name      Ready  Status    Restarts     Age 
    <Pod Name>    1/1   Running      0          5 
```
The first entry is the name of your pod, Copy the pods name by highlighting it and either using ctrl c, cmd c, or highlighting with the mouse and right clicking and selecting copy.

<h4>Step 2</h4>
Enter the pod using the following command.

<code>$kubectl -n <name space> exec -it $<$pod name$>$ -- /bin/bash</code>

Now inside the pod run the following command.

<code>$service start ssh</code>

You should get a response that a open ssh service is being started. 
<h4>Step 3</h4>
Now you can exit the pod and run the following command. 

<code>$kubectl port-forward <pod name> <port number> -n <name space></code>

From here you should see a bunch of output about ports being forwarded and finally the ip address at which you can access them which is simply the localhost ip.

<h4>Step 4</h4>
Open a new terminal and enter in the following command. 

<code>$ssh -Y root@localhost</code>

<h4>Step 5</h4>
It will then ask you to enter a password the default password is 123.

<h4>Step 6</h4>
You are now in the pod enter in the following command.

<code>$cd /../unity-editor-2017.2.0f1/Editor<code>

<h4>Step 7</h4>
Now enter in the following command. 

<code>$./Unity</code>

<h4>Done</h4>